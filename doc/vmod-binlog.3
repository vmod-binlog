.\" This file is part of Vmod-binlog -*- nroff -*-
.\" Copyright (C) 2013-2022 Sergey Poznyakoff
.\"
.\" Vmod-binlog is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vmod-binlog is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
.TH VMOD-BINLOG 3 "April 9, 2020" "VMOD-BINLOG" "User Reference"
.SH NAME
vmod\-binlog \- binary log file support for Varnish Cache.
.SH SYNOPSIS
.B import binlog;

.BI "VOID binlog.init(STRING " dir ", STRING " format ", STRING " param ");"

.B VOID binlog.start();

.BI "VOID binlog.pack(STRING " arg ");"

.B VOID binlog.commit();
.SH DESCRIPTION
This module adds binary log files support to
.BR "Varnish Cache" .
.PP
Binary log file consists of an arbitrary number of equally sized
records.  Each record contains a UNIX timestamp in binary form and
user-defined data.  The file begins with a header containing auxiliary
information.
.PP
Binary log files are rotated at preconfigured time intervals.  In
order to speed up searches, their names follow the
.BR strftime (3)
pattern:
.PP
.EX
%Y%m%dT%H%M%S.log
.EE
.PP
where conversion specifiers refer to the time when the log was
created.  The logs are stored in an indexed database structure.  Three
levels of indexing are supported:
.TP
.B 0
Directories indexed by the year of creation.  E.g. the log for
September 22, 2013 is stored in

.EX
$dir/2013/20130922T000000.log
.EE

(where \fB$dir\fR refers to the top-level storage directory).  This is
the default indexing scheme.
.TP
.B 1
Directories indexed by the year and month of creation.  E.g.:

.EX
$dir/2013/09/20130922T000000.log
.EE
.TP
.B 2
Directories indexed by the year, month and day.  E.g.:

.EX
$dir/2013/09/22/20130922T000000.log
.EE
.PP
Binary logs can be viewed using the
.BR binlogcat (1)
and
.BR binlogsel (1)
utilities.
.SH FUNCTIONS
Binary log support is initialized by a call to
.B binlog.init
function (normally it is done in
.BR vcl_init ).
.PP
The
.B dir
argument specifies the top-level storage directory.  The
.B format
argument defines the format of logfile entries.  It is
a simplified form of Perl
.B pack()
template.  See the
.B FORMAT
section below for a detailed description.  Finally,
.B param
is a semicolon-separated list of parameters.  Each parameter is
defined as a key/value pair, with a single equals sign in between.
No whitespace is allowed on either side of the equals sign.
The following parameters are defined:
.TP
\fBdebug\fR=\fINUMBER\fR
Set debugging level.
.TP
\fBpattern\fR=\fISTRING\fR
Sets pattern to use for log file names, instead of the default.
The argument can contain
.BR strftime (3)
conversion specifiers.  The default is

.EX
%Y%m%dT%H%M%S.log
.EE
.TP
\fBindex\fR=\fINUMBER\fR
Sets directory indexing scheme to use.  Allowed values are \fB0\fR,
\fB1\fR, and \fB2\fR.  See the description of indexing schemes above.
.TP
\fBsize\fR=\fISIZE\fR
Sets maximum size of a single binary log.  Any records not fitting
into \fISIZE\fR will be discarded.  \fISIZE\fR can be followed by
one of the usual size suffixes: \fBK\fR, \fBM\fR, \fBG\fR (or their
lower-case equivalents), standing for kilobytes, megabytes and
gigabytes, correspondingly.

Logs are mapped into memory, so
the \fISIZE\fR is limited.  See
.BR mmap (2),
for the details.

The default value is 1G.
.TP
\fBinterval\fR=\fINUMBER\fR
Sets log rotation interval, in seconds.  The default is 86400.
.TP
\fBumask\fR=\fIONUM\fR
Sets umask for creating new log files (a three-digit octal value).
The default is \fB077\fR.
.TP
\fBroundts\fR=\fIBOOL\fR
If set to \fB1\fR, timestamps used for generating log file names will
be rounded to the nearest rotation interval.  E.g., if the log file
was created at 01:05:18 on September 22, 2013 and \fBroundts=1\fR is
in effect, the log will be named \fB20130922T000000.log\fR, instead of
\fB20130922T010518.log\fR.
.TP
\fBreuselog\fR=\fIBOOL\fR
If the file to be created already exists upon \fBvarnishd\fR startup, the
module will try to append new data to it, provided that its format
matches the one actually configured.  This avoids loss of data upon
restarting \fBvarnishd\fR.  Setting \fBreuselog=0\fR disables this
behavior.
.PP
A new log record is initialized by 
.BR binlog.start() .
This call should be followed by as many calls to
.B binlog.pack()
as there are conversions in the format string.  Each call to
.B binlog.pack()
converts its argument according to the next unused format specifier
and stores it into the record being constructed.  Finally, calling
.B binlog.commit()
writes the record to the log file.
.PP
For example, consider the format specification
.PP
.EX
L Z256
.EE
.PP
(i.e. a 32-bit unsigned long value, followed by a string of up to 255
characters).  Given this, the following sequence of \fBVCL\fR commands
can be used to construct the log entry from the HTTP headers:
.PP
.EX
binlog.start();
binlog.pack(http.X-Id);
binlog.pack(http.req.url);
binlog.commit();
.EE
.SH FORMAT
Data format specification consists of a series of conversion
specifiers, optionally separated by any amount of whitespace.
A conversion specifier consists of a template letter
optionally followed by numeric repeat count (which may be enclosed in
square brackets).  
.PP
The valid template letters are:
.TP
.BI Z[ N ]
A null-terminated (ASCIZ) string of at most N-1 characters, will be
null padded.  The repeat count is mandatory.
.TP
.B c
A signed char (8-bit) value.
.TP
.B s
A signed short (16-bit) value.
.TP
.B S
An unsigned short value.
.TP
.B l
A signed long (32-bit) value.
.TP
.B L
An unsigned long value.
.TP
.B q
A signed quad (64-bit) value.
.TP
.B Q
An unsigned quad value.
.TP
.B i
A signed integer value.
.TP
.B I
A unsigned integer value.
.TP
.B n
An unsigned short (16-bit) in "network" (big-endian) order.
.TP
.B N
An unsigned long (32-bit) in "network" (big-endian) order.
.TP
.B v
An unsigned short (16-bit) in "VAX" (little-endian) order.
.TP
.B V
An unsigned long (32-bit) in "VAX" (little-endian) order.
.TP   
.B f
A single-precision float in native format.
.TP
.B d
A double-precision float in native format.
.TP   
.B x
A null byte (a.k.a ASCII NUL, "\000", chr(0))
.TP
.B X
Back up a byte.
.TP
.B @
Null-fill or truncate to absolute position, counted from the
current position.
.TP
.B .
Null-fill or truncate to absolute position specified by
the repeat count.


.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.	ds package vmod-binlog
.	ds version 2.7
.	so download.inc
\}
.SH "SEE ALSO"
.BR binlogcat (1),
.BR binlogsel (1),
.BR vcl (7),
.BR varnishd (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2013-2022 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

	      
