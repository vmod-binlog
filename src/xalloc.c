/* This file is part of vmod-binlog
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-binlog is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-binlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include "xalloc.h"
#include "err.h"

void *
xmalloc(size_t s)
{
	void *p = malloc(s);
	if (!p) {
		error("out of memory");
		exit(1);
	}
	return p;
}

void *
xcalloc(size_t count, size_t size)
{
	void *p;
	
	size *= count;
	p = xmalloc(count * size);
	memset(p, 0, size);
	return p;
}

void *
xmemdup(void const *p, size_t s)
{
	return memcpy(xmalloc(s), p, s);
}

char *
xstrdup(const char *s)
{
	return xmemdup(s, strlen(s) + 1);
}

 
