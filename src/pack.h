/* This file is part of vmod-binlog
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-binlog is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-binlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>

struct packenv {
	char *buf_base;
	size_t buf_size;
	size_t buf_pos;
	char **argv;
	int argc;
	int argi;
	FILE *fp;
};

struct packinst;

struct packinst *packcomp(const char *s, char **endp);
struct packinst *packdup(struct packinst *s);
void packfree(struct packinst *pi);
struct packinst *packinit(struct packinst *pi);
void packin(struct packinst *pi, struct packenv *env);
void packout(struct packinst *pi, struct packenv *env);
struct packinst *packinnext(struct packinst *pi, struct packenv *env);

size_t packsize(struct packinst *pi);
struct packenv *packenv_create(size_t size);
void packenv_init(struct packenv *env);
void packenv_free(struct packenv *env);

void packerror(const char *, ...);





