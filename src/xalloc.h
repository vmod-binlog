void *xmalloc(size_t s);
void *xmemdup(void const *p, size_t s);
void *xcalloc(size_t count, size_t size);
char *xstrdup(const char *s);
