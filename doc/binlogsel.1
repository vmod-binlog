.\" This file is part of Vmod-binlog -*- nroff -*-
.\" Copyright (C) 2013-2018 Sergey Poznyakoff
.\"
.\" Vmod-binlog is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vmod-binlog is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
.TH BINLOGSEL 1 "October 19, 2013" "BINLOGSEL" "User Reference"
.SH NAME
binlogsel \- select records from binary logs
.SH SYNOPSIS
\fBbinlogsel\fR [\fB\-dnv\fR] [\fB\-D\fR \fIDIR\fR] [\fB\-i \fIN\fR]\
 [\fB\-p\fR \fIPATTERN\fR]\ [\fB\-t\fR \fIFORMAT\fR]
          [\fB\-I\fR \fITAG\fR] [\fB\-F\fR \fITIME\fR] [\fB\-T\fR \fITIME\fR]\
 
          [\fB\-L\fR \fIDIR\fR] [\fB\-P\fR \fIDIR\fR]\
 [\fB\-m\fR '\fIMODULE\fR[ \fIARGS\fR]']
          [\fIFILE\fR...]

\fBbinlogsel\fR [\fB\-hV\fR]
.SH DESCRIPTION
.B Binlosel
scans binary logs created by
.BR vmod\-binlog (3)
module and selects from them the records that were created within the given
time intervals.  A time interval is specified using the
.BI \-F " FROMTIME"
and
.BI \-T " TOTIME"
options.  If both options are used, only records falling within the
interval \fIFROMTIME\fR..\fITOTIME\fR will be returned.  If
.B \-F
is used, but
.B \-T
is omitted, \fITOTIME\fR is set to the current time.  If
.B \-T
is used, but
.B \-F
is omitted, the starting time is set to the time of the earliest
available log file.  Finally, if neither of options is used, all
records will be output.  In this case, the operation is equivalent
to that of the
.BR binlogcat (1)
command.
.PP
The argument to either option is a mostly free format human readable
date string such as "Sun, 29 Feb 2004 16:21:42 -0800" or "2004-02-29
16:21:42"  or even "2 days ago".  It may contain items indicating
calendar date, time of day, time zone, day of week, relative  time,
relative date, and numbers.  An empty string indicates the beginning
of the day.  For a detailed description of date input formats, see
section
.B Date input formats
in
.BR "GNU Coreutils manual" .
If the
.B info
program and coreutils documentation are properly installed at your site, use
the following command to view it:
.PP
.EX
.B info coreutils 'Date input formats'
.EE
.PP
Several time intervals can be specified, provided that each of them is
preceded by the \fB\-I\fR option, which introduces the tag to identify
the records pertaining to that interval.  This tag is
output before each record.  For example:
.PP
.EX
binlogsel -I 1h -F '1 hour ago' -I 2d -F '2 days ago' 
.EE
.PP
This command selects two intervals: records added within the last hour,
which will be prefixed on the output with the string \fB1h\fR, and
records added within the last 2 days, which are prefixed by the string
\fB2d\fR.
.PP
The log files are searched in the directory specified with the
\fB\-D\fR command line option.  The \fB\-i\fR option can be used to
define directory indexing level.  See
.BR vmod\-binlog (3),
for a description of the underlying directory structure.
.PP
If files are listed in the command line, the
.BR \-D ,
.BR \-i ,
and
.B \-p
options are ignored.
.PP
The default action of
.B binlogsel
is to print matching records on the standard output.  This can be
changed by the use of loadable modules.  A module is a dynamic library
which is loaded at program startup and provides functions for handling
records in a specific way.
.PP
The module to be loaded is supplied with the \fB\-m\fR option.  For
example, the option \fB-m modname\fR instructs
.B binlogsel
to load library \fBmodname.so\fR.  Additional arguments for the module
initialization function can be supplied in the same option (note quoting):
.PP
.EX
.B binlogsel -m 'modname -n arg'
.EE
.PP
The module to be loaded is searched in the library path, which
consists initially of the single directory
\fI$prefix\fR\fB/lib/vmod\-binlog\fR.  This path can be modified using
the
.BI \-L " DIR"
option, which adds \fIDIR\fR to its end, or by the
.BI \-P " DIR"
option, which inserts its argument immediately before the default
library directory.
.PP
See the section \fBLOADABLE MODULES\fR for the discussion of how to
write loadable modules.
.SH OPTIONS
.TP
.BI \-D " DIR"
Set base log directory.
.TP
.B \-d
Print timestamps relative to first record in the file.
.TP
.BI \-F " TIME"
Select records newer than \fITIME\fR.  See the section
\fBDESCRIPTION\fR for a discussion of valid time formats.
.TP
.BI \-I " TAG"
Defines the tag for the \fB\-F\fR and \fB\-T\fR options that
immediately follow it.
.TP
.BI \-i " N"
Set directory indexing level.  Valid values for \fIN\fR are \fB0\fR,
\fB1\fR and \fB2\fR.
.TP
.BR \-h
Print a short help summary and exit.
.TP
.BI \-L " DIR"
Add \fIDIR\fR to the end of the library search path.
.TP
\fB\-m\fR \fB'\fR\fIMODULE\fR[ \fIARGS\fR]\fB'\fR
Load \fIMODULE\fR.
.TP
.B \-n
Precede each record by its number in the file (0-based).
.TP
.BI \-P " DIR"
Insert \fIDIR\fR into the library search path immediately before the
default library directory.
.TP
.BI \-p " PATTERN"
Set pattern for log file names.  \fIPATTERN\fR must be a valid
.BR strftime (3)
format string.
.TP
.BI \-T " TIME"
Select records older than \fITIME\fR.
.TP
.BI \-t " FORMAT"
Format timestamps according to \fIFORMAT\fR (see
.BR strftime (3)).
Default is \fB%c\fR.
.TP
.BR \-V
Show program version and exit.
.TP
.B \-v
Print information about each file before dumping it.
.SH LOADABLE MODULES
Symbols exported from a loadable module must begin with
\fImodname\fB_LTX_\fR, where \fImodname\fR stands for the name of the
module without suffix (\fB.so\fR, \fB.la\fR, etc.).  In the discussion
below, the symbols
.B binlogsel
looks for are listed without this prefix.  Thus, for example, if the
module name is \fBstats.so\fR, the name of the module initialization
function must be \fBstats_LTX_init\fR.
.PP
.B Binlogsel
expects modules to export the following functions:
.TP
.BI "void init(char *" param ", void (*" addfn ")(const char *, const char *, const char *));"
.B [OPTIONAL]
Initializes the module.  The first argument points to the arguments
supplied to the module in the command line.  The \fBaddfn\fR function
can be used to add new time intervals.  Its usage is:

.EX
.BI "void addfn(const char *" tag ", const char *" start ", const char *" end ")"
.EE

where \fItag\fR is the interval tag, and \fIstart\fR and \fIend\fR
supply interval start and end times, correspondingly.
.TP
.BI "void done(" void ");"
.B [OPTIONAL]
Called immediately before
.B binlogsel
terminates.
.TP
.BI "int open(const char *" file ", size_t " recsize ", const char *" format ");"
.B [OPTIONAL]
This function is called immediately after opening the new log file.
The name of the file is given by the \fIfile\fR argument.  The
\fIrecsize\fR argument gives the record size, and \fIformat\fR points
to the data format specification.

The function must return 0 to indicate success and non-null value on errors.
.TP
.BI "void close(" void ");"
.B [OPTIONAL]
Called before closing the log file.
.TP
.BI "void record(const char *" tag ", time_t " ts ", void * " data ");"
.B [MANDATORY]
This function processes the record pointed to by \fIdata\fR.  The
\fItag\fR argument points to the interval tag (can be \fBNULL\fR).
Timestamp of the record is given by the \fIts\fR argument.
.SH "SEE ALSO"
.BR vmod\-binlog (3),
.BR binlogcat (1),
.BR vcl (7),
.BR varnishd (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2013-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

	      
