/* This file is part of vmod-binlog
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-binlog is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-binlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
*/
extern const char *progname;

void setprogname(const char *);
void error(const char *fmt, ...)
	__attribute__ ((__format__ (__printf__, 1, 2)));
void packerror(const char *fmt, ...)
	__attribute__ ((__format__ (__printf__, 1, 2)));
void version(void);
