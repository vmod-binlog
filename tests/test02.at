# This file is part of vmod-binlog testsuite -*- autotest -*-
# Copyright (C) 2013-2018 Sergey Poznyakoff
#
# Vmod-binlog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Vmod-binlog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([log rotation])
AT_KEYWORDS([test02 rotation])

AT_CHECK([
cwd=`pwd`
mkdir log

cat > test.vtc <<EOT
varnishtest "Log rotation"

server s1 {
       rxreq
       txresp
} -start

varnish v1 -vcl+backend {
        import binlog from "$abs_top_builddir/src/.libs/libvmod_binlog.so";
        sub vcl_init {
		binlog.init("$cwd/log","LL","size=1M;interval=10;roundts=1;pattern=%s.log");
        }
        sub vcl_fini {
                binlog.close();
        }
        sub vcl_recv {
		binlog.start();
		binlog.pack(req.http.X-nid);
		binlog.pack(req.http.X-aid);
		binlog.commit();
                return (hash);
        }
} -start

client c1 {
	txreq -url / -hdr "X-nid:1" -hdr "X-aid:0"
	rxresp
        expect resp.status == 200

	txreq -url / -hdr "X-nid:1" -hdr "X-aid:1"
	rxresp
        expect resp.status == 200
}

client c1 -run
EOT

sed s/X-nid:1/X-nid:2/ test.vtc > test1.vtc

$VARNISHTEST test.vtc | sed 's/^#.*TEST test.vtc passed.*/OK/'
sleep 11
$VARNISHTEST test1.vtc | sed 's/^#.*TEST test1.vtc passed.*/OK/'

find log -name '*.log' | sort | while read file
do
	echo "# file"
	binlogcat -d $file
done
],
[0],
[OK
OK
# file
0 1 0
0 1 1
# file
0 2 0
0 2 1
])

AT_CLEANUP

