/* This file is part of vmod-binlog
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-binlog is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-binlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "pack.h"
#include "err.h"

int
main(int argc, char **argv)
{
	enum { mode_packin, mode_packout } mode = mode_packin;
	struct packinst *pi;
	struct packenv *env;
	char *end;
	int c;

	setprogname(argv[0]);
	
	while ((c = getopt(argc, argv, "d")) != EOF) {
		switch (c) {
		case 'd':
			mode = mode_packout;
			break;
		default:
			exit(1);
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		error("not enough arguments");
		exit(1);
	}

	pi = packcomp(argv[0], &end);
	if (!pi) {
		if (errno == EINVAL) {
			error("%s: bad dataspec near %s", argv[0], end);
			exit(1);
		}
		
		error("%s", strerror(errno));
		exit(1);
	}
	env = packenv_create(packsize(pi));
	if (!env)  {
		error("out of memory");
		abort();
	}
		
	switch (mode) {
	case mode_packin:
		env->argv = argv + 1;
		env->argc = argc - 1;
		packin(pi, env);
		fwrite(env->buf_base, env->buf_size, 1, stdout);
		break;

	case mode_packout:
		env->fp = stdout;
		fread(env->buf_base, env->buf_size, 1, stdin);
		packout(pi, env);
		fputc('\n', stdout);
		break;
	}
	
	packenv_free(env);
	packfree(pi);
}
