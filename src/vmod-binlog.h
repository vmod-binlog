/* This file is part of vmod-binlog
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-binlog is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-binlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-binlog.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdint.h>

#define SIZE_T_MAX ((size_t)~0)

#ifndef BINLOG_SIZE
# define BINLOG_SIZE (1024*1024*1024)
#endif

#ifndef BINLOG_PATTERN
# define BINLOG_PATTERN "%Y%m%dT%H%M%S.log"
#endif

#ifndef BINLOG_GLOB_PATTERN
# define BINLOG_GLOB_PATTERN "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]T[0-9][0-9][0-9][0-9][0-9][0-9].log"
#endif

#ifndef BINLOG_INDEX
# define BINLOG_INDEX "year"
#endif

#ifndef BINLOG_INTERVAL
# define BINLOG_INTERVAL 86400
#endif

#ifndef BINLOG_UMASK
# define BINLOG_UMASK 0077
#endif

enum binlog_index_type {
	index_year,
	index_month,
	index_day,
	
};
#define index_last index_day

struct binlog_record {
	time_t ts;           /* timestamp */
	char data[1];        /* payload */
};

#define BINLOG_MAGIC_STR "NXCBINLOG"
#define BINLOG_MAGIC_LEN (sizeof(BINLOG_MAGIC_STR) - 1)
#define BINLOG_VERSION 0x00010000UL

struct binlog_file_header {
	char magic[BINLOG_MAGIC_LEN];
	char pad[16 - BINLOG_MAGIC_LEN];
	uint32_t version;
	size_t hdrsize;
	size_t recsize;
	size_t recnum;
/*	char format[X];*/
};

#define binlog_size(hdr) ((hdr)->hdrsize + (hdr)->recnum * (hdr)->recsize)

